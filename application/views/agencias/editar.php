<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <h1  class="text-center">Editar Agencia</h1>
      <form class="" method="post" action="<?php echo site_url('Agencias/actualizarAgencia'); ?>">
        <input type="hidden" name="id_age" id="id_age"
      	value="<?php echo $agenciaEditar->id_age; ?>">
        <label for="">
          <b>Nombre del Director:</b>
        </label>
        <input type="text" name="nombre_age" id="nombre_age"
      	value="<?php echo $agenciaEditar->nombre_age; ?>"
        placeholder="Ingrese el nombre..." class="form-control" required>
        <br>
        <label for="">
          <b>Direccion:</b>
        </label>
        <input type="text" name="direccion_age" id="direccion_age"
      	value="<?php echo $agenciaEditar->direccion_age; ?>"
        placeholder="Ingrese el nombre..." class="form-control" required>
        <br>
        <label for="">
          <b>Telefono:</b>
        </label>
        <input type="text" name="telefono_age" id="telefono_age"
      	value="<?php echo $agenciaEditar->telefono_age; ?>"
        placeholder="Ingrese el telefono..." class="form-control" required>

          <div class="row">
            <div class="col-md-6">
              <br>
              <label for="">
              <b>Latitud:</b>
            </label>
            <input type="number" name="latitud_age" id="latitud_age"
      			value="<?php echo $agenciaEditar->latitud_age; ?>"
            placeholder="Ingrese el latitud..." class="form-control" readonly>

            </div>
            <div class="col-md-6">
              <br>
              <label for="">
              <b>Longitud:</b>
            </label>
            <input type="number" name="longitud_age" id="longitud_age"
      			value="<?php echo $agenciaEditar->longitud_age; ?>"
            placeholder="Ingrese el longitud..." class="form-control" readonly>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <div id="mapa" style="height: 250px; width:100%; border:1px solid blue;">

              </div>
            </div>

          </div>
          <br>
          <br>
          <div class="row">
            <div class="col-md-12 text-center">
              <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
              <a href="<?php echo site_url('agencias/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>

            </div>

          </div>
        <!-- Contenido del formulario -->
      </form>
    </div>
  </div>
</div>

<script type="text/javaScript">
  function initMap(){
    var coordenadaCentral =
		new google.maps.LatLng(<?php echo $agenciaEditar->latitud_age; ?>, <?php echo $agenciaEditar->longitud_age; ?>);
   var miMapa= new google.maps.Map(
     document.getElementById('mapa'),{
       center: coordenadaCentral,
       zoom: 10,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
   );
   var marcador= new google.maps.Marker({
     position:coordenadaCentral,
     map: miMapa,
     title: 'Seleccione la ubicacion',
     draggable:true
   });
   google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud_age').value=latitud;
      document.getElementById('longitud_age').value=longitud;
    }
   );
  }

</script>

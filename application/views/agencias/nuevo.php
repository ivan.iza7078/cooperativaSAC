<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <h1 class="text-center">Nueva Agencia</h1>
      <form class="" action="<?php echo site_url('agencias/guardarAgencias') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_cliente"  >
        <label for=""> <b>Nombre del Director:</b> </label>
        <input type="text" name="nombre_age" id="nombre_age"class="form-control" placeholder="Ingrese el nombre del Director de la Agencia">
        <br>
        <label for=""> <b>Direccion:</b> </label>
        <input type="text" name="direccion_age" id="direccion_age"class="form-control" placeholder="Ingrese la direccion de la Agencia ">
        <br>
        <label for=""> <b>Telefono:</b> </label>
        <input type="number" name="telefono_age" id="telefono_age"class="form-control" placeholder="Ingrese el telefono de la Agencia ">
        <label for=""> <b>Fotografia:</b> </label>
        <input type="file" name="foto_age" id="foto_age" class="form-control" required accept="image/*">
        <br>
        <div class="row">
          <div class="col-md-6">
            <br>
            <label for=""><b>Latitud:</b> </label>
            <input type="number" name="latitud_age" id="latitud_age"class="form-control" placeholder="Ingrese la latitud" readonly>

          </div>
          <div class="col-md-6">
            <br>
            <label for=""> <b>Longitud:</b> </label>
            <input type="number" name="longitud_age" id="longitud_age"class="form-control" placeholder="Ingrese la longuitud" readonly>
          </div>
          <div class="row">
            <div class="col-md-12">
              <br>
              <div id="mapa" style= "height:250px; width:100%; border:1px solid blue;">

              </div>

            </div>
          </div>
          <br>
          <div class=" row">
            <div class="col-md-12 text-center">
              <br>
              <button type="submit" name="button" class="btn btn-primary"> <i class="fa fa-thumbs-up" aria-hidden="true"></i> Guardar</button>
              <a href="<?php echo site_url('agencias/index'); ?>" class="btn btn-danger"><i class="fa fa-thumbs-down" aria-hidden="true"></i>Cancelar </a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
   function initMap(){
     var coordenadaCentral=new  google.maps.LatLng(-1.2538824892685865,-78.62522496154902);
     var miMapa=new google.maps.Map(document.getElementById('mapa'),
     {
       center:coordenadaCentral, zoom:8, mapTypeId:google.maps.MapTypeId.ROADMAP
     }
    );
     var marcador =new google.maps.Marker({
       position:coordenadaCentral,map:miMapa,title:'Seleccionar Ubicacion', draggable:true

     });
     google.maps.event.addListener(
       marcador,'dragend',
       function(event){
         var latitud=this.getPosition().lat();
         var longuitud=this.getPosition().lng();
         //alert(latitud+" -- "+longuitud);
         document.getElementById('latitud_age').value=latitud;
         document.getElementById('longitud_age').value=longuitud;

       }
     );
   }

</script>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>CooperativaSAC</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="<?php echo base_url('recurso/'); ?>img/favicon.ico" rel="icon">


    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>


    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">


    <!-- Libraries Stylesheet -->
    <link href="<?php echo base_url('recurso/'); ?>lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url('recurso/'); ?>lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo base_url('recurso/'); ?>lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo base_url('recurso/'); ?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="<?php echo base_url('recurso/'); ?>css/style.css" rel="stylesheet">

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWlUq9ZOeGAHsdIzBHrXwiFsoZSOQGh4A&libraries=places&callback=initMap">
    </script>
     <!-- Importacion de Jquery -->
    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
     <!-- importacion de fontasame iconos-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
     <!-- Importacion de sweetalert-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.3/dist/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.3/dist/sweetalert2.min.css">
</head>

<body>
    <!-- Spinner Start -->
    <div id="spinner"
        class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner-border text-primary" role="status" style="width: 3rem; height: 3rem;"></div>
    </div>
    <!-- Spinner End -->


    <!-- Topbar Start -->
    <div class="container-fluid bg-primary text-white d-none d-lg-flex">
        <div class="container py-3">
            <div class="d-flex align-items-center">
                <a href="index.html">
                    <h2 class="text-white fw-bold m-0">Cooperativa SAC</h2>
                </a>
                <div class="ms-auto d-flex align-items-center">
                    <small class="ms-4"><i class="fa fa-map-marker-alt me-3"></i>-593 UTC,Latacunga, Ecuador</small>
                    <small class="ms-4"  href= "https://mail.google.com/mail/u/0/?pli=1#inbox"><i class="fa fa-envelope me-3"></i>pablo.pullopaxi1155@utc.edu.ec</small>
                    <small class="ms-4"><i class="fa fa-phone-alt me-3"></i>0998638968</small>
                    <div class="ms-3 d-flex">
                        <a class="btn btn-sm-square btn-light text-primary rounded-circle ms-2" href="https://www.facebook.com/cooperativasacoficial"><i
                                class="fab fa-facebook-f"></i></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->


    <!-- Navbar Start -->
    <div class="container-fluid bg-white sticky-top">
        <div class="container">
            <nav class="navbar navbar-expand-lg bg-white navbar-light p-lg-0">
                <a href="index.html" class="navbar-brand d-lg-none">
                    <h1 class="fw-bold m-0">CooperativaSAC</h1>
                </a>
                <button type="button" class="navbar-toggler me-0" data-bs-toggle="collapse"
                    data-bs-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav">
                        <a href="<?php echo site_url(); ?>" class="nav-item nav-link active">Inicio</a>
                        <a href="<?php echo site_url('agencias/index'); ?>" class="nav-item nav-link">Agencias</a>
                        <a href="<?php echo site_url('cajeros/index'); ?>" class="nav-item nav-link">Cajeros</a>
                        <a href="<?php echo site_url('corresponsables/index'); ?>" class="nav-item nav-link">Corresponsables</a>

                        <a href="<?php echo site_url('reportes/index'); ?>" class="nav-item nav-link">Reporte Mapa</a>
                    </div>
                    <div class="ms-auto d-none d-lg-block">
                        <a href="" class="btn btn-primary rounded-pill py-2 px-3">Get A Quote</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!-- Navbar End -->
    <!-- flash_dat crear la  variables(nombre)-->
<div class="container">
  <?php if ($this->session->flashdata('confirmacion')): ?>
  <script type="text/javascript">
    Swal.fire({
    title: "Confirmacion!",
    text: "<?php echo $this->session->flashdata('confirmacion'); ?>",
    icon: "success"
    });


  </script>
<?php  $this->session->set_flashdata('confirmacion',''); ?>
<?php endif; ?>
</div>

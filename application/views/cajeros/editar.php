<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <h1 class="text-center">Editar Cajero Automático</h1>
      <form method="post" action="<?php echo site_url('cajeros/actualizarCajero'); ?>" enctype="multipart/form-data">
        <input type="hidden" name="ID_Cajero" value="<?php echo $cajeroEditar->ID_Cajero; ?>">
        <label for=""><b>Nombre:</b></label>
        <input type="text" name="Nombre" id="Nombre" value="<?php echo $cajeroEditar->Nombre; ?>" class="form-control" placeholder="Ingrese el nombre del cajero automático" required>
        <br>
        <label for=""><b>Marca:</b></label>
        <input type="text" name="Marca" id="Marca" value="<?php echo $cajeroEditar->Marca; ?>" class="form-control" placeholder="Ingrese la marca del cajero automático" required>
        <br>
        <label for=""><b>Modelo:</b></label>
        <input type="text" name="Modelo" id="Modelo" value="<?php echo $cajeroEditar->Modelo; ?>" class="form-control" placeholder="Ingrese el modelo del cajero automático" required>
        <br>
        <label for=""><b>Latitud:</b></label>
        <input type="number" name="latitud" id="latitud" value="<?php echo $cajeroEditar->latitud; ?>" class="form-control" placeholder="Ingrese la latitud del cajero automático" readonly>
        <br>
        <label for=""><b>Longitud:</b></label>
        <input type="number" name="longitud" id="longitud" value="<?php echo $cajeroEditar->longitud; ?>" class="form-control" placeholder="Ingrese la longitud del cajero automático" readonly>
        <br>

        <br>
        <div class="row">
          <div class="col-md-12">
            <div id="mapa" style="height: 250px; width:100%; border:1px solid blue;"></div>
          </div>
        </div>
        <br>
        <br>
        <div class="row">
          <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> Actualizar</button>
            <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger">Cancelar</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(<?php echo $cajeroEditar->latitud; ?>, <?php echo $cajeroEditar->longitud; ?>);
    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Seleccione la ubicación',
      draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>

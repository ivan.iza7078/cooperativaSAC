<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <h1 class="text-center">Nuevo Cajero Automático</h1>
      <form action="<?php echo site_url('cajeros/guardarCajeros') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_cajero">
        <label for=""><b>Nombre:</b></label>
        <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese el nombre del cajero automático">
        <br>
        <label for=""><b>Marca:</b></label>
        <input type="text" name="marca" id="marca" class="form-control" placeholder="Ingrese la marca del cajero automático">
        <br>
        <label for=""><b>Modelo:</b></label>
        <input type="text" name="modelo" id="modelo" class="form-control" placeholder="Ingrese el modelo del cajero automático">
        <br>

        <label for=""><b>Foto:</b></label>
        <input type="file" name="foto" id="foto" class="form-control" required accept="image/*">
        <br>

        <div class="row">
          <div class="col-md-6">

        <label for=""><b>Latitud:</b></label>
        <input type="number" name="latitud" id="latitud" class="form-control" placeholder="Ingrese la latitud del cajero automático" readonly>
        <br>
        </div>
        <div class="col-md-6">
        <label for=""><b>Longitud:</b></label>
        <input type="number" name="longitud" id="longitud" class="form-control" placeholder="Ingrese la longitud del cajero automático" readonly>
        </div>
        <div class="row">
          <div class="col-md-12">
            <br>
            <div id="mapa" style="height:250px; width:100%; border:1px solid blue;"></div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-12 text-center">
            <br>
            <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Guardar</button>
            <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"><i class="fa fa-thumbs-down" aria-hidden="true"></i> Cancelar</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(-1.2538824892685865, -78.62522496154902);
    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Seleccionar Ubicación',
      draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>

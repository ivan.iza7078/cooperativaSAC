<h1 class="text-center"><i class="fa-solid fa-people-group"></i> Corresponsables</h1>
<div class="row">
  <div class="col-md-12 text-end">
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
      <i class="fa fa-eye"></i>Ver Mapa
    </button>
    <a href="<?php echo site_url('corresponsables/nuevo'); ?>" class="btn btn-outline-success">
      <i class="fa fa-plus-circle"></i> Agregar Corresponsable</a>
    <br> <br>
  </div>
</div>

<?php if ($listadoCorresponsables): ?>
    <table class="table table-bordered">
        <thead>
              <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>CARGO</th>
                <th>LATITUD</th>
                <th>LONGUITUD</th>
                <th>FOTO</th>
                <th>ACCIONES</th>
              </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoCorresponsables as $corresponsable): ?>
                <tr>
                  <td><?php echo $corresponsable->ID_Corresponsable; ?></td>
                  <td><?php echo $corresponsable->Nombre; ?></td>
                  <td><?php echo $corresponsable->Apellido; ?></td>
                  <td><?php echo $corresponsable->Cargo; ?></td>
                  <td><?php echo $corresponsable->latitud; ?></td>
                  <td><?php echo $corresponsable->longitud; ?></td>
                  <td>
                    <?php if ($corresponsable->foto!=""): ?>
                      <img src="<?php echo base_url('uploads/corresponsables/').$corresponsable->foto; ?>"
                      height="100px" alt="">
                    <?php else: ?>
                      N/A
                    <?php endif; ?>
                  </td>
                  <td>
                    <a href="<?php echo site_url('corresponsables/editar/').$corresponsable->ID_Corresponsable; ?>"
                         class="btn btn-warning"
                         title="Editar">
                      <i class="fa fa-pen"></i>
                    </a>
                    <a href="<?php echo site_url('corresponsables/borrar/').$corresponsable->ID_Corresponsable; ?>"
                         class="btn btn-danger"
                         title="Eliminar">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
              <i class="fa fa-eye"></i> Mapa De Corresponsables
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div id="reporteMapa" style="height:300px;width:100%; border:2px solid blue ;">

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-bs-dismiss="modal"><i  class="fa fa-times"></i>Cerrar</button>

          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      function initMap(){
        var coordenadaCentral = new google.maps.LatLng(-1.2538824892685865, -78.62522496154902);
        var miMapa = new google.maps.Map(
          document.getElementById('reporteMapa'),
          {
            center: coordenadaCentral,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          }
        );

        <?php foreach ($listadoCorresponsables as $corresponsable): ?>
          var coordenadaTemporal = new google.maps.LatLng(<?php echo $corresponsable->latitud; ?>, <?php echo $corresponsable->longitud; ?>);
          var icono = {
                url: '<?php echo base_url ('assets/img/corresponsables.png'); ?>', // URL de la imagen que deseas utilizar como marcador
                scaledSize: new google.maps.Size(30, 30), // Tamaño de la imagen
          };
          var marcador = new google.maps.Marker({
            position: coordenadaTemporal,
            map: miMapa,
            title: '<?php echo $corresponsable->Nombre; ?>',
            icon: icono
          });
        <?php endforeach; ?>
      }
    </script>

<?php else: ?>
  <div class="alert alert-danger">
      No se encontraron corresponsables registrados
    </div>
<?php endif; ?>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <h1 class="text-center">Editar Corresponsable</h1>
      <form method="post" action="<?php echo site_url('corresponsables/actualizarCorresponsable'); ?>" enctype="multipart/form-data">
        <input type="hidden" name="ID_Corresponsable" value="<?php echo $corresponsableEditar->ID_Corresponsable; ?>">
        <label for=""><b>Nombre:</b></label>
        <input type="text" name="Nombre" id="Nombre" value="<?php echo $corresponsableEditar->Nombre; ?>" class="form-control" placeholder="Ingrese el nombre del corresponsable" required>
        <br>
        <label for=""><b>Apellido:</b></label>
        <input type="text" name="Apellido" id="Apellido" value="<?php echo $corresponsableEditar->Apellido; ?>" class="form-control" placeholder="Ingrese el apellido del corresponsable" required>
        <br>
        <label for=""><b>Cargo:</b></label>
        <input type="text" name="Cargo" id="Cargo" value="<?php echo $corresponsableEditar->Cargo; ?>" class="form-control" placeholder="Ingrese el cargo del corresponsable" required>
        <br>
        <label for=""><b>Latitud:</b></label>
        <input type="number" name="latitud" id="latitud" value="<?php echo $corresponsableEditar->latitud; ?>" class="form-control" placeholder="Ingrese la latitud del cajero automático" readonly>
        <br>
        <label for=""><b>Longitud:</b></label>
        <input type="number" name="longitud" id="longitud" value="<?php echo $corresponsableEditar->longitud; ?>" class="form-control" placeholder="Ingrese la longitud del cajero automático" readonly>
        <br>
        <br>
        <div class="row">
          <div class="col-md-12">
            <div id="mapa" style="height: 250px; width:100%; border:1px solid blue;"></div>
          </div>
        </div>

        <br>
        <br>
        <div class="row">
          <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> Actualizar</button>
            <a href="<?php echo site_url('corresponsables/index'); ?>" class="btn btn-danger">Cancelar</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(<?php echo $corresponsableEditar->latitud; ?>, <?php echo $corresponsableEditar->longitud; ?>);
    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Seleccione la ubicación',
      draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>

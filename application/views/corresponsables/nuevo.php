<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <h1 class="text-center">Nuevo Corresponsable</h1>
      <form action="<?php echo site_url('corresponsables/guardarCorresponsable') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_corresponsable">
        <label for=""><b>Nombre:</b></label>
        <input type="text" name="Nombre" id="Nombre" class="form-control" placeholder="Ingrese el nombre del corresponsable">
        <br>
        <label for=""><b>Apellido:</b></label>
        <input type="text" name="Apellido" id="Apellido" class="form-control" placeholder="Ingrese el apellido del corresponsable">
        <br>
        <label for=""><b>Cargo:</b></label>
        <input type="text" name="Cargo" id="Cargo" class="form-control" placeholder="Ingrese el cargo del corresponsable">
        <br>
        <label for=""><b>Latitud:</b></label>
        <input type="number" name="latitud" id="latitud" class="form-control" placeholder="Ingrese la latitud  de la Ubicación del Corresponsable" readonly>
        <br>
        <label for=""><b>Longitud:</b></label>
        <input type="number" name="longitud" id="longitud" class="form-control" placeholder="Ingrese la longitud de la Ubicación del Corresponsable" readonly>
        <br>
        <label for=""><b>Foto:</b></label>
        <input type="file" name="foto" id="foto" class="form-control" required accept="image/*">
        <br>
        <div class="row">
          <div class="col-md-12">
            <br>
            <div id="mapa" style="height:250px; width:100%; border:1px solid blue;"></div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-12 text-center">
            <br>
            <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Guardar</button>
            <a href="<?php echo site_url('corresponsables/index'); ?>" class="btn btn-danger"><i class="fa fa-thumbs-down" aria-hidden="true"></i> Cancelar</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(-1.2538824892685865, -78.62522496154902);
    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Seleccionar Ubicación',
      draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend', function(event){
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>

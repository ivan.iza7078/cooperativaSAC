<?php
class Corresponsable extends CI_Model
 {
   function __construct()
   {
     parent::__construct();
   }
   //Insertar nuevos hospitales
   function insertar($datos){
     $respuesta=$this->db->insert("corresponsables",$datos);
     return $respuesta;
   }
   //Consulta de datos
   function consultarTodos(){
     $corresponsable=$this->db->get("corresponsables");
     if ($corresponsable->num_rows()>0) {
       return $corresponsable->result();
     } else {
       return false;
     }
   }
   //eliminacion de hospital por id
   function eliminar($id){
       $this->db->where("ID_Corresponsable",$id);
       return $this->db->delete("corresponsables");
   }
   //consulta de un solo hospital
   function obtenerPorId($id){
      $this->db->where("ID_Corresponsable",$id);
      $agencia=$this->db->get("corresponsables");
      if ($agencia->num_rows()>0) {
        return $agencia->row();
      } else {
        return false;
      }
    }

    // funcion para actualizar hospitales
    function actualizar($id,$datos){
      $this->db->where("ID_Corresponsable",$id);
      return $this->db
                  ->update("corresponsables",$datos);
    }




 }//Fin de la clase
?>

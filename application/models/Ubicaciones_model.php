<?php


class Ubicaciones_model extends CI_Model {

    function get_agencias() {
        // Consulta para recuperar las ubicaciones de las agencias
        $query = $this->db->get('agencia');
        return $query->result();
    }
    public function get_cajeros() {
        // Consulta para recuperar las ubicaciones de los cajeros
        $query = $this->db->get('cajeros_automaticos');
        return $query->result();
    }
    public function get_corresponsables() {
        // Consulta para recuperar las ubicaciones de los corresponsables
        $query = $this->db->get('corresponsables');
        return $query->result();
    }


}
?>

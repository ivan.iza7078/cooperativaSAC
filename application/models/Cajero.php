<?php
class Cajero extends CI_Model
 {
   function __construct()
   {
     parent::__construct();
   }
   //Insertar nuevos hospitales
   function insertar($datos){
     $respuesta=$this->db->insert("cajeros_automaticos",$datos);
     return $respuesta;
   }
   //Consulta de datos
   function consultarTodos(){
     $agencias=$this->db->get("cajeros_automaticos");
     if ($agencias->num_rows()>0) {
       return $agencias->result();
     } else {
       return false;
     }
   }
   //eliminacion de hospital por id
   function eliminar($id){
       $this->db->where("ID_Cajero",$id);
       return $this->db->delete("cajeros_automaticos");
   }
   //consulta de un solo hospital
   function obtenerPorId($id){
      $this->db->where("ID_Cajero",$id);
      $agencia=$this->db->get("cajeros_automaticos");
      if ($agencia->num_rows()>0) {
        return $agencia->row();
      } else {
        return false;
      }
    }

    // funcion para actualizar hospitales
    function actualizar($id,$datos){
      $this->db->where("ID_Cajero",$id);
      return $this->db
                  ->update("cajeros_automaticos",$datos);
    }




 }//Fin de la clase
?>

<?php
/**
 *
 */
class Corresponsables extends CI_Controller
{

  function __construct()
  {
    // code...
    parent::__construct();
    $this->load->model("Corresponsable");
  }
  public function index(){
    $data["listadoCorresponsables"]=
    $this->Corresponsable->consultarTodos();

    $this->load->view("header");
    $this->load->view("corresponsables/index",$data);
    $this->load->view("footer");
  }
  public function borrar($ID_Cajero){
    $this->Corresponsable->eliminar($ID_Cajero);
    $this->session->set_flashdata("confirmacion","Cajero eliminado exitosamnete");
    redirect("corresponsables/index");

  }

  //renderizacion del formulrio de nuevo hospitales
  public function nuevo(){
    $this->load->view("header");
    $this->load->view("corresponsables/nuevo");
    $this->load->view("footer");
  }
  public function guardarCorresponsable(){

    //INICIO PROCESO DE SUBIDA DE ARCHIVO
      $config['upload_path']=APPPATH.'../uploads/corresponsables/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="corresponsable_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("foto")){ //intentando subir el archivo
         $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
         $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
      }else{
        $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
      }
     $datosNuevoCorresponsables=array(
      "Nombre"=>$this->input->post("Nombre"),
      "Apellido"=>$this->input->post("Apellido"),
      "Cargo"=>$this->input->post("Cargo"),
      "latitud"=>$this->input->post("latitud"),
      "longitud"=>$this->input->post("longitud"),
      "foto"=>$nombre_archivo_subido

    );
    $this->Corresponsable->insertar($datosNuevoCorresponsables);
    //flash_dat -<crear una sesion de tipo flash
    $this->session->set_flashdata("confirmacion","Cajero guardado exitosamnete");
    enviarEmail("pablo.pullopaxi1155@utc.edu.ec","CREACION",
          "<h1>Se creo  el cajero  de:</h1>".$datosNuevoCorresponsables['Nombre']);
     redirect('corresponsables/index');
  }
  //Renderizar el formulario de edicion
    public function editar($id){
      $data["corresponsableEditar"]=$this->Corresponsable->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("corresponsables/editar",$data);
      $this->load->view("footer");
  }
  public function actualizarCorresponsable(){
      $ID_Corresponsable=$this->input->post("ID_Corresponsable");
      $datoscorrsponsables=array(
        "Nombre"=>$this->input->post("Nombre"),
        "Apellido"=>$this->input->post("Apellido"),
        "Cargo"=>$this->input->post("Cargo"),
        "latitud"=>$this->input->post("latitud"),
        "longitud"=>$this->input->post("longitud"),
      );
      $this->Corresponsable->actualizar($ID_Corresponsable,$datoscorrsponsables);
      $this->session->set_flashdata("confirmacion",
      "Cajero actualizado exitosamente");
      redirect('corresponsables/index');
    }




}// cierre de la clase
?>

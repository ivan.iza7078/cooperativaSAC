<?php
/**
 *
 */
class Agencias extends CI_Controller
{

  function __construct()
  {
    // code...
    parent::__construct();
    $this->load->model("Agencia");
  }
  public function index(){
    $data["listadoAgencias"]=
    $this->Agencia->consultarTodos();

    $this->load->view("header");
    $this->load->view("agencias/index",$data);
    $this->load->view("footer");
  }
  public function borrar($id_age){
    $this->Agencia->eliminar($id_age);
    $this->session->set_flashdata("confirmacion","Agencia eliminado exitosamnete");
    redirect("agencias/index");

  }

  //renderizacion del formulrio de nuevo hospitales
  public function nuevo(){
    $this->load->view("header");
    $this->load->view("agencias/nuevo");
    $this->load->view("footer");
  }
  public function guardarAgencias(){

    //INICIO PROCESO DE SUBIDA DE ARCHIVO
      $config['upload_path']=APPPATH.'../uploads/agencias/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="Agencia_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("foto_age")){ //intentando subir el archivo
         $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
         $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
      }else{
        $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
      }
     $datosNuevoAgencias=array(
      "nombre_age"=>$this->input->post("nombre_age"),
      "direccion_age"=>$this->input->post("direccion_age"),
      "telefono_age"=>$this->input->post("telefono_age"),
      "latitud_age"=>$this->input->post("latitud_age"),
      "longitud_age"=>$this->input->post("longitud_age"),
      "foto_age"=>$nombre_archivo_subido

    );
    $this->Agencia->insertar($datosNuevoAgencias);
    //flash_dat -<crear una sesion de tipo flash
    $this->session->set_flashdata("confirmacion","Agencia guardado exitosamnete");
    enviarEmail("pablo.pullopaxi1155@utc.edu.ec","CREACION",
          "<h1>Se creo  la Agencia  de:</h1>".$datosNuevoAgencias['nombre_age']);
     redirect('agencias/index');
  }
  //Renderizar el formulario de edicion
    public function editar($id){
      $data["agenciaEditar"]=$this->Agencia->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("agencias/editar",$data);
      $this->load->view("footer");
  }
  public function actualizarAgencia(){
      $id_hos=$this->input->post("id_age");
      $datosAgencia=array(
        "nombre_age"=>$this->input->post("nombre_age"),
        "direccion_age"=>$this->input->post("direccion_age"),
        "telefono_age"=>$this->input->post("telefono_age"),
        "latitud_age"=>$this->input->post("latitud_age"),
        "longitud_age"=>$this->input->post("longitud_age")
      );
      $this->Agencia->actualizar($id_age,$datosAgencia);
      $this->session->set_flashdata("confirmacion",
      "Agencia actualizado exitosamente");
      redirect('agencias/index');
    }




}// cierre de la clase
?>

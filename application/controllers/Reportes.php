<?php
/**
 *
 */
class Reportes extends CI_Controller
{

  function __construct()
  {
    // code...
    parent::__construct();
    $this->load->model("Ubicaciones_model");
  }
  public function index(){
    // Obtener las ubicaciones de agencias, cajeros y corresponsables desde el modelo
    $data['agencias'] =
    $this->Ubicaciones_model->get_agencias();
    $data['cajeros'] =
    $this->Ubicaciones_model->get_cajeros();
    $data['corresponsables'] =
    $this->Ubicaciones_model->get_corresponsables();
    // Cargar la vista
    $this->load->view("header");
    $this->load->view('reportes/index', $data);
    $this->load->view("footer");

  }
}// cierre de la clase
?>

<?php
/**
 *
 */
class Cajeros extends CI_Controller
{

  function __construct()
  {
    // code...
    parent::__construct();
    $this->load->model("Cajero");
  }
  public function index(){
    $data["listadoCajeros"]=
    $this->Cajero->consultarTodos();

    $this->load->view("header");
    $this->load->view("cajeros/index",$data);
    $this->load->view("footer");
  }
  public function borrar($ID_Cajero){
    $this->Cajero->eliminar($ID_Cajero);
    $this->session->set_flashdata("confirmacion","Cajero eliminado exitosamnete");
    redirect("cajeros/index");

  }

  //renderizacion del formulrio de nuevo hospitales
  public function nuevo(){
    $this->load->view("header");
    $this->load->view("cajeros/nuevo");
    $this->load->view("footer");
  }
  public function guardarCajeros(){

    //INICIO PROCESO DE SUBIDA DE ARCHIVO
      $config['upload_path']=APPPATH.'../uploads/cajeros/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="cajero_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("foto")){ //intentando subir el archivo
         $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
         $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
      }else{
        $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
      }
     $datosNuevoCajeros=array(
      "nombre"=>$this->input->post("nombre"),
      "marca"=>$this->input->post("marca"),
      "modelo"=>$this->input->post("modelo"),
      "latitud"=>$this->input->post("latitud"),
      "longitud"=>$this->input->post("longitud"),
      "foto_caje"=>$nombre_archivo_subido

    );
    $this->Cajero->insertar($datosNuevoCajeros);
    //flash_dat -<crear una sesion de tipo flash
    $this->session->set_flashdata("confirmacion","Cajero guardado exitosamnete");
    enviarEmail("pablo.pullopaxi1155@utc.edu.ec","CREACION",
          "<h1>Se creo  el cajero  de:</h1>".$datosNuevoCajeros['nombre']);
     redirect('cajeros/index');
  }
  //Renderizar el formulario de edicion
    public function editar($id){
      $data["cajeroEditar"]=$this->Cajero->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("cajeros/editar",$data);
      $this->load->view("footer");
  }
  public function actualizarCajero(){
      $ID_Cajero=$this->input->post("ID_Cajero");
      $datoscajeros=array(
        "Nombre"=>$this->input->post("Nombre"),
        "Marca"=>$this->input->post("Marca"),
        "Modelo"=>$this->input->post("Modelo"),
        "latitud"=>$this->input->post("latitud"),
        "longitud"=>$this->input->post("longitud"),
      );
      $this->Cajero->actualizar($ID_Cajero,$datoscajeros);
      $this->session->set_flashdata("confirmacion",
      "Cajero actualizado exitosamente");
      redirect('cajeros/index');
    }




}// cierre de la clase
?>
